`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01/28/2016 11:05:10 PM
// Design Name: 
// Module Name: ALU1bit
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
//include "mux1bit2to1.v";
include "mux4to1.v";

module ALU1bit(a,b,less,op,g,p,adder,cin,cout,set,result);
input a,b,cin,less;
input [2:0] op;
output result,g,p,adder,cout,set;
output result1,result2;
wire [2:0] op;
wire a,b;
reg zero;
mux1bit2to1 MD(a,~a,op[0],result1);
mux1bit2to1 ME(b,~b,op[1],result2);
assign g = (result1 & result2);
assign p = (result1 | result2);
assign adder = (result1 ^ result2 ^ cin);
assign cout = (result1 & result2) | ((result1 ^ result2) & cin);
mux4to1 func(g,p,adder,less,op[0],op[1],result);
always @(result or set)
begin
if(result == 0)
    assign zero = 0;
end
assign set = zero;
endmodule
