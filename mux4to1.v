`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01/26/2016 03:10:45 PM
// Design Name: 
// Module Name: mux4to1
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
//'include mux1bit2to1.v
module mux4to1(a, b, c, d, op,results);
    output results;
    input c;
    input d;
    input [1:0]op;
    input a;
    input b;
    wire temp,tmp;
    mux1bit2to1 MA(a,b,op[0],temp);
    mux1bit2to1 MB(c,d,op[0],tmp);
    mux1bit2to1 MC(temp,tmp,op[1],results);
endmodule
  


